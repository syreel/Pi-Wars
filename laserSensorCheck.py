#!/usr/bin/python3.4
"""
Script for testing the lasers to ensure they are all working

Usage:
    laserSensorCheck.py [--config <filename>] [--distances]
    laserSensorCheck.py -h | --help

Options:
    -c --config <filename>  Define custom config file for the script [default: config.cfg].
    -d --distances          Display a sequence of distances
    -h --help               Display help info for this script.
"""

import time
import LaserLibrary
import docopt
import configparser

args = docopt.docopt(__doc__, help=True)

config = configparser.RawConfigParser()
config.read(args["--config"])

LASERS = LaserLibrary.lasers(config)


if LASERS.leftSensor.get_distance() != -1:
    print("Left Connected")
else:
    print("Left sensor not connected!!")
time.sleep(LASERS.timing / 1000000.00)

if LASERS.centrelSensor.get_distance() != -1:
    print("Centre Left Connected")
else:
    print("Centre Left sensor not connected!!")
time.sleep(LASERS.timing / 1000000.00)

if LASERS.centrerSensor.get_distance() != -1:
    print("Centre Right Connected")
else:
    print("Centre Right sensor not connected!!")
time.sleep(LASERS.timing / 1000000.00)

if LASERS.leftSensor.get_distance() != -1:
    print("Right Connected")
else:
    print("Right sensor not connected!!")
time.sleep(LASERS.timing / 1000000.00)


if args["--distances"]:
    for count in range(1, 200):
        distance1 = LASERS.leftSensor.get_distance()
        distance2 = LASERS.centrelSensor.get_distance()
        distance3 = LASERS.centrerSensor.get_distance()
        distance4 = LASERS.rightSensor.get_distance()

        print(
            "Left: " + str(distance1) +
            " Centre Left: " + str(distance2) +
            " Centre Right: " + str(distance3) +
            " Right: " + str(distance4)
        )

        time.sleep(LASERS.timing / 1000000.00)
        time.sleep(0.5)

LASERS.cleanup(config)
