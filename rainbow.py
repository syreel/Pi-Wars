#!/usr/bin/python3.4
"""
Script to drive the robot around the inside of a square

Usage:
    rainbow.py [--config <filename>]
    rainbow.py -h | --help

Options:
    -c --config <filename>  Define custom config file for the script [default: config.cfg].
    -h --help               Display help info for this script.
"""
import time
import MotorLibrary
import LaserLibrary
import docopt
import configparser

args = docopt.docopt(__doc__, help=True)

config = configparser.RawConfigParser()
config.read(args["--config"])

MOTORS = MotorLibrary.management(config)
MOTORS.stop()

LASERS = LaserLibrary.lasers(config)

# Setting the inital values for the system so it goes forwards by default
left = 20
forwards = 40
right = 20

wallHit = False
# Main Script Loop
while True:
    # Read sensor input
    left = LASERS.leftSensor.get_distance() / 10
    centrel = LASERS.centrelSensor.get_distance() / 10
    right = LASERS.rightSensor.get_distance() / 10

    # Ensure that we get to the wall before attempting to skirt the edge
    while wallHit is False:
        print ("First started")
        MOTORS.move(0.6, 0.6)
        time.sleep(0.10)

        if left <= 17:
            wallHit = True
            while centrel < 30:
                print ("First right turn")
                MOTORS.move(0.5, -0.5)
                time.sleep(0.20)
            print ("Found first wall, continuing")


    # If statement for each movement outcome
    if forwards > 17 and (left > 10 and left < 20):
        dire = "f"

    elif forwards > 17 and left < 10:
        dire = "r"

    elif forwards > 17 and left > 20:
        dire = "l"

    elif forwards <= 17:
        while centrel < 17:
            print ("Attempting to rotate to avoid wall")
            MOTORS.move(0.5, -0.5)
            time.sleep(0.20)
    
    else:
        print ("Uncaught option")

    # Pulls the data from above and actions the movement
    if dire == "f":  # Forwards
        MOTORS.move(0.6, 0.6)
        time.sleep(0.10)

    elif dire == "l":  # Left
        MOTORS.move(-0.6, 0.6)
        time.sleep(0.10)

    else:  # Right
        MOTORS.move(0.5, -0.5)
        time.sleep(0.20)

    # Debug output
    print(
        "Dire:" + dire +
        " Left: " + str(left) +
        " Centre Left: " + str(forwards) +
        " Right: " + str(right)
    )
