"""
Script to make the robot follow a line based on a Pixy2 camera

Usage:
    lineFollow.py [--config <filename>]
    lineFollow.py -h | --help

Options:
    -c --config <filename>  Define custom config file for the script [default: config.cfg].
    -h --help               Display help info for this script.
"""
import pixy
import ctypes
import docopt
import configparser
import MotorLibrary

args = docopt.docopt(__doc__, help=True)

pixy.init()
pixy.change_prog("line")


class Vector (ctypes.Structure):
    _fields_ = [
        ("m_x0", ctypes.c_uint),
        ("m_y0", ctypes.c_uint),
        ("m_x1", ctypes.c_uint),
        ("m_y1", ctypes.c_uint),
        ("m_index", ctypes.c_uint),
        ("m_flags", ctypes.c_uint)]


config = configparser.RawConfigParser()
config.read(args["--config"])

# Create an instance of the motor library and stop the motors
MOTORS = MotorLibrary.management(config)
MOTORS.stop()

vectors = pixy.VectorArray(100)
frame = 0

while 1:
    pixy.line_get_main_features()
    v_count = pixy.line_get_vectors(100, vectors)

    if v_count > 0:
        for index in range(0, v_count):
            xval = vectors[index].m_x1
            if xval <= 15:
                # Hard Left
                print("Hard Left")
                MOTORS.move(-0.6, 0.6)
            elif xval <= 35:
                # Soft Left
                print("Soft Left")
                MOTORS.move(0.6, 0.2)
            elif xval >= 46 and xval <= 65:
                # Soft Right
                print("Soft Right")
                MOTORS.move(0.2, 0.6)
            elif xval > 65:
                # Hard Right
                print("Hard Right")
                MOTORS.move(0.6, -0.6)
            else:
                print("centre")
                MOTORS.move(0.5, 0.5)
