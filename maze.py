#!/usr/bin/python3.4
"""
Script to drive the robot around the maze

Usage:
    maze.py [--config <filename>]
    maze.py -h | --help

Options:
    -c --config <filename>  Define custom config file for the script [default: config.cfg].
    -h --help               Display help info for this script.
"""
import time
import MotorLibrary
import LaserLibrary
import docopt
import configparser

args = docopt.docopt(__doc__, help=True)

config = configparser.RawConfigParser()
config.read(args["--config"])

MOTORS = MotorLibrary.management(config)
MOTORS.stop()

LASERS = LaserLibrary.lasers(config)

# Setting the inital values for the system so it goes forwards by default
left = 20
centrel = 200
centrer = 200
right = 20

# Main Script Loop
while True:
    # Read sensor input
    left = LASERS.leftSensor.get_distance() / 10
    centrel = LASERS.centrelSensor.get_distance() / 10
    centrer = LASERS.centrerSensor.get_distance() / 10
    right = LASERS.rightSensor.get_distance() / 10

    # Decide movement outcome based on sensor input
    if centrel > 70 and centrer > 70 and left > 2 and right > 2:
        print ("Turbo")
        dire = "f"
    elif centrel > 40 and centrer > 40 and left > 10 and right > 10:
        dire = "f"

    elif left > right:
        dire = "l"

    else:
        dire = "r"

    # Pulls the data from above and actions the movement
    if dire == "f":  # Forwards
        MOTORS.move(0.4, 0.4)
        time.sleep(0.04)

    elif dire == "l":  # Left
        MOTORS.move(-0.4, 0.5)
        time.sleep(0.05)

    else:  # Right
        MOTORS.move(0.5, -0.4)
        time.sleep(0.05)

    # Debug output
    print(
        "Dire:" + dire +
        " Left: " + str(left) +
        " Centre Left: " + str(centrel) +
        " Centre Left: " + str(centrer) +
        " Right: " + str(right)
    )
