#!/usr/bin/python3.4
"""
Script to drive the robot around the maze

Usage:
    newmaze.py [--config <filename>]
    newmaze.py -h | --help

Options:
    -c --config <filename>  Define custom config file for the script [default: config.cfg].
    -h --help               Display help info for this script.
"""
import time
import MotorLibrary
import docopt
import configparser
import ctypes
import pixy

args = docopt.docopt(__doc__, help=True)


config = configparser.RawConfigParser()
config.read(args["--config"])

config = configparser.RawConfigParser()
config.read(args["--config"])

# Create an instance of the motor library and stop the motors
MOTORS = MotorLibrary.management(config)
MOTORS.stop()

pixy.init ()
pixy.change_prog ("color_connected_components")

sigToFollow = 3
sizeStop = 8000

class Blocks (ctypes.Structure):
  _fields_ = [ ("m_signature", ctypes.c_uint),
    ("m_x", ctypes.c_uint),
    ("m_y", ctypes.c_uint),
    ("m_width", ctypes.c_uint),
    ("m_height", ctypes.c_uint),
    ("m_angle", ctypes.c_uint),
    ("m_index", ctypes.c_uint),
    ("m_age", ctypes.c_uint) ]

blocks = pixy.BlockArray(2000)
red, blue, yellow, green = 4, 3, 2, 1
order = [red, blue, yellow, green]

sigToFollow = order[0]

while 1:
    count = pixy.ccc_get_blocks (100, blocks)
    if count > 0:
        for index in range (0, count):
            if blocks[index].m_signature == sigToFollow:
                size = blocks[index].m_width * blocks[index].m_height
                x, y = blocks[index].m_x - (blocks[index].m_width / 2), blocks[index].m_y

                print ("Target Seen at X=%d Y=%d Size=%d" % (blocks[index].m_x, blocks[index].m_y, size))
                if size < sizeStop:
                    if x < 70:
                        print ("left")
                        MOTORS.move(-0.5, 0.5)
                    elif x > 150:
                        print("right")
                        MOTORS.move(0.5, -0.5)
                    else:
                        print("straight")
                        MOTORS.move(0.3, 0.3)
                else:
                    MOTORS.stop()
                    order.pop(0)
                    sigToFollow = order[0]

            else:
                print("Not seeing correct")
                MOTORS.move(0.4, -0.4)
    else:
        print("Not seeing anything")
        MOTORS.move(0.4, -0.4)
