#!/usr/bin/python3.4
"""
Remote control script for robot

Usage:
    controller.py [--verbose] [--reverse] [--config <filename>]
    controller.py -h | --help

Options:
    -c --config <filename>  Define custom config file for the script [default: config.cfg].
    -v --verbose            Enable debug output
    -r --reverse            Reverse robot (used for golf and PiNoon)
    -h --help               Display help info for this script.
"""

import os
import time
from evdev import InputDevice, list_devices, ecodes
import docopt
import configparser
import MotorLibrary
import RPi.GPIO as GPIO

# Set the GPIO numbering type to BOARD
GPIO.setmode(GPIO.BOARD)

args = docopt.docopt(__doc__, help=True)

config = configparser.RawConfigParser()
config.read(args["--config"])

GUN = {
    "pin": config.getint("GUN", "pin"),
    "base": config.getfloat("GUN", "base"),
    "actuate": config.getfloat("GUN", "actuate"),
    "delay": config.getfloat("GUN", "delay"),
    "laser": config.getint("GUN", "laser")
}


def clamp(my_value, min_value, max_value):
    return max(min(my_value, max_value), min_value)


# PS4 Accelerometer identifiers
AXIS_X = 3
AXIS_Y = 4

# variables we'll store the rotations in, initialised to zero
rot_x = 0.0
rot_y = 0.0
forwardDown = False
backwardsDown = False
headingX = 0
forwardTrigger = 0
backwardsTrigger = 0


# Scanning to ensure controller is connected
print ("Searching for controller: ", end="")
devices = [InputDevice(fn) for fn in list_devices()]
for dev in devices:
    if dev.name == 'Wireless Controller':
        print("Found")
        break
    else:
        print ("Not Found, Aborting...")
        exit()


# Create object for the motors
MOTORS = MotorLibrary.management(config)

def move(direction, magnitude):
    direction *= 1.9

    if direction <= 0:
        leftValue = magnitude - abs(direction) * magnitude
        rightValue = magnitude

    else:
        leftValue = magnitude
        rightValue = magnitude - abs(direction) * magnitude

    leftValue = round(leftValue, 2)
    rightValue = round(rightValue, 2)

    if args["--verbose"]:
        print(str(leftValue) + " : " + str(rightValue))

    if args["--reverse"]:
        leftValue = leftValue * -1
        rightValue = rightValue * -1

    #if leftValue > 1:
    #    leftValue = 1
    #if rightValue > 1:
    #    rightValue = 1

    print(str(leftValue) + " : " + str(rightValue))
    MOTORS.move(leftValue, rightValue)


armed = False # Gun armed status
lastUpdate = 0

def getTime():
    return int(round(time.time() * 1000))


try: # To catch a CTRL+C escape sequence
    while True: # Main Loop
        try:
            for event in dev.read():
                if event.type == ecodes.EV_KEY:
                    if event.code == 304:
                        print("X button")

                    if event.code == 313:
                        # Forwards Trigger
                        forwardDown = event.value

                        if not forwardDown:
                            MOTORS.stop()

                    if event.code == 312:
                        # Backwards Trigger
                        backwardsDown = event.value

                        if not backwardsDown:
                            MOTORS.stop()

                    if event.code == 311:
                        # Right Bumper - Turn right on the spot
                        if event.value:
                            MOTORS.move(1, -1)
                        else:
                            MOTORS.stop()

                    if event.code == 310:
                        # Left Bumper - Left right on the spot
                        if event.value:
                            MOTORS.move(-1, 1)
                        else:
                            MOTORS.stop()

                    if event.code == 315:
                        # Options button - Enable/Disable firing of gun servo
                        if event.value:
                            if armed is False:
                                print ("Arming")

                                GPIO.setup(GUN["pin"], GPIO.OUT) # Set pin to output mode
                                servocont = GPIO.PWM(GUN["pin"], 50) # Create PWM on pin
                                servocont.start(GUN["base"]) # Send trigger to base position

                                GPIO.setup(GUN["laser"], GPIO.OUT) # Set pin to output mode
                                GPIO.output(GUN["laser"], 1) # Enable laser
                                armed = True
                                time.sleep(0.5)
                            else:
                                print ("Disarming")
                                servocont.stop()
                                GPIO.output(GUN["laser"], 0)
                                armed = False
                                time.sleep(0.5)

                    if event.code == 317:
                        # Trackpad Press - Fire gun servo
                        if armed is True:
                            servocont.ChangeDutyCycle(GUN["actuate"])
                            print ("FIRE!!!")
                            time.sleep(GUN["delay"])
                            servocont.ChangeDutyCycle(GUN["base"])

                if event.type == ecodes.EV_ABS:
                    # Handling for the turning joystick
                    if event.code in ecodes.ABS:
                        symbol = ecodes.ABS[event.code]

                        if symbol == "ABS_X":
                            headingX = (event.value - 130) / 255.0
                            headingX = headingX * -1
                            if args["--reverse"]:
                                headingX = headingX * -1

                        if symbol == "ABS_RZ":
                            scale = event.value / 255.0
                            forwardTrigger = scale

                        if symbol == "ABS_Z":
                            scale = event.value / 255.0
                            backwardsTrigger = scale
        except IOError:
            pass
        if getTime() - lastUpdate > 10:
            if forwardDown and forwardTrigger > 0.1:
                    move(headingX, forwardTrigger)
            elif backwardsDown and backwardsTrigger > 0.1:
                    move(headingX, -backwardsTrigger)

            lastUpdate = getTime()

except KeyboardInterrupt: # Graceful exit sequence
        # Stop and shutdown motors
        MOTORS.stop()
        MOTORS.cleanup()

        # Display Debug Information
        os.system("clear")
        print ("Keyboard interrupt detected, Exiting...")

        # Quit program
        exit(0)
