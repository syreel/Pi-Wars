#!/usr/bin/python3.4
""" Library for controlling TOF sensors through the pi's I2C """
import RPi.GPIO as GPIO
import time
import configparser
import VL53L0X  # Library for using laser sensors


class lasers():
    SENSORS = {}

    def __init__(self, config):
        # Define the pins of the laser sensor shutdown pins
        SENSORS = {
            "left": config.getint("SENSORS", "left"),
            "centrel": config.getint("SENSORS", "centrel"),
            "centrer": config.getint("SENSORS", "centrer"),
            "right": config.getint("SENSORS", "right")
        }

        # ===== BEGIN Sensor Initialisation =====
        # Set the GPIO numbering type to BOARD
        GPIO.setmode(GPIO.BOARD)
        GPIO.setwarnings(False)

        # GPIO for Sensor 1 shutdown pin
        leftSensorShutdown = SENSORS['left']
        # GPIO for Sensor 2 shutdown pin
        centrelSensorShutdown = SENSORS['centrel']
        # GPIO for Sensor 3 shutdown pin
        centrerSensorShutdown = SENSORS['centrer']
        # GPIO for Sensor 4 shutdown pin
        rightSensorShutdown = SENSORS['right']

        # Setup GPIO for shutdown pins on each VL53L0X
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(leftSensorShutdown, GPIO.OUT)
        GPIO.setup(centrelSensorShutdown, GPIO.OUT)
        GPIO.setup(centrerSensorShutdown, GPIO.OUT)
        GPIO.setup(rightSensorShutdown, GPIO.OUT)

        # Set all shutdown pins low to turn off each VL53L0X
        GPIO.output(leftSensorShutdown, GPIO.LOW)
        GPIO.output(centrelSensorShutdown, GPIO.LOW)
        GPIO.output(centrerSensorShutdown, GPIO.LOW)
        GPIO.output(rightSensorShutdown, GPIO.LOW)

        # Keep all low for 500 ms or so to make sure they reset
        time.sleep(0.50)

        # Create one object per VL53L0X passing the address to give to
        # each.
        self.leftSensor = VL53L0X.VL53L0X(address=0x2B)
        self.centrelSensor = VL53L0X.VL53L0X(address=0x2C)
        self.centrerSensor = VL53L0X.VL53L0X(address=0x2D)
        self.rightSensor = VL53L0X.VL53L0X(address=0x2f)

        # Set shutdown pin high for the first VL53L0X then
        # call to start ranging
        GPIO.output(leftSensorShutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.leftSensor.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        # Set shutdown pin high for the second VL53L0X then
        # call to start ranging
        GPIO.output(centrelSensorShutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.centrelSensor.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)
        
        GPIO.output(centrerSensorShutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.centrerSensor.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)


        GPIO.output(rightSensorShutdown, GPIO.HIGH)
        time.sleep(0.50)
        self.rightSensor.start_ranging(VL53L0X.VL53L0X_BETTER_ACCURACY_MODE)

        # Get timing delay interval
        self.timing = self.leftSensor.get_timing()
        if (self.timing < 20000):
            self.timing = 20000
        # ===== END Sensor Initialisation =====

    def cleanup(self, config):
        self.__init__(config)
        self.leftSensor.stop_ranging()
        GPIO.output(self.SENSORS["left"], GPIO.LOW)
        self.centrelSensor.stop_ranging()
        GPIO.output(self.SENSORS["centrel"], GPIO.LOW)
        self.centrerSensor.stop_ranging()
        GPIO.output(self.SENSORS["centrer"], GPIO.LOW)
        self.rightSensor.stop_ranging()
        GPIO.output(self.SENSORS["right"], GPIO.LOW)
